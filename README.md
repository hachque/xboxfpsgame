Hey guys
===========

This is a README file.  I'll add instructions in here
on how to do stuff with the repo...

Installing
=============

Cygwin
-------------

  * Install Cygwin from <http://cygwin.com/install.html>.  Install
    it to C:\UNIX.
  * When in the Cygwin installer, select the following packages
    in the install process (you can use the search thing):
    * git
    * openssh
  * Finish the rest of the Cygwin install process.
  * Start Cygwin so that it sets itself up, but then close it.

Git (Extensions)
-----------------

  * Go and install Git Extensions from <http://code.google.com/p/gitextensions/>.
  * Start Git Extensions.
  * You'll probably see a Settings dialog pop-up on first run.  If it doesn't
    then go Settings -> Settings in the menus.
  * In the Git tab, you need set the paths to:
    * Command used: `C:\UNIX\bin\git.exe`
    * Path to: `C:\UNIX\bin\`
  * The HOME should point to `C:\UNIX\home\username`.  If it doesn't, change it
    so it does.
  * On the Global Settings tab, set the following settings:
      User name: <your name>
      User email: <your email that you used to sign up for BitBucket>
  * On the SSH tab, select "OpenSSH".

Generate SSH key
------------------
Open Cygwin (close Git Extensions if it's still running) and you'll
see a command prompt.

Read all of the next bit before actually doing it.

Follow the instructions at <http://help.github.com/win-set-up-git/#_set_up_ssh_keys>,
skipping over the bit about "Git Bash" and going straight to the commands in step 1.

Despite it telling you that you should have a passphrase, I wouldn't worry about it
too much so feel free to just give it a blank passphrase at that point.

Step 4 is still relevant even though it's talking about GitHub.  Instead, you need
to go to the URL <https://bitbucket.org/account/user/username/ssh-keys/>, replacing
username with your BitBucket username.

Then test to see if it worked type at the Cygwin prompt:

```bash
ssh git@bitbucket.org
```

That should show it connecting successfully (but it will disconnect you).

**If you have problems at all during any of the processes above, just ask James
for help...**

Clone the repository
----------------------

So, now we're actually done setting up everything, we can now clone the repository
pretty easily.

  * Open Git Extensions.
  * Under Common Actions there's "Clone repository".
  * Click that and in "Repository to clone:" put `git@bitbucket.org:hachque/xboxfpsgame.git`.
  * The destination is where you want the repository to be put.

