﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace XboxFPSGame
{
    public abstract class Drawable : IUpdateable, IDrawable
    {
        private bool m_Enabled = true;
        private bool m_Visible = true;
        private int m_UpdateOrder = 0;
        private int m_DrawOrder = 0;

        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> VisibleChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;
        public event EventHandler<EventArgs> DrawOrderChanged;

        public abstract void Update(GameTime time);
        public abstract void Draw(GameTime time);

        public bool Enabled
        {
            get { return this.m_Enabled; }
            set { this.m_Enabled = value; if (this.EnabledChanged != null) this.EnabledChanged(this, new EventArgs()); }
        }

        public bool Visible
        {
            get { return this.m_Visible; }
            set { this.m_Visible = value; if (this.VisibleChanged != null) this.VisibleChanged(this, new EventArgs()); }
        }

        public int UpdateOrder
        {
            get { return this.m_UpdateOrder; }
            set { this.m_UpdateOrder = value; if (this.UpdateOrderChanged != null) this.UpdateOrderChanged(this, new EventArgs()); }
        }

        public int DrawOrder
        {
            get { return this.m_DrawOrder; }
            set { this.m_DrawOrder = value; if (this.DrawOrderChanged != null) this.DrawOrderChanged(this, new EventArgs()); }
        }

        public GameState GameState
        {
            get;
            private set;
        }

        protected Drawable(GameState state)
        {
            this.GameState = state;
            if (this.GameState == null)
                throw new ArgumentNullException("state");
        }
    }
}
