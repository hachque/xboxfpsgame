﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace XboxFPSGame
{
    public class Pool<K, V> : IEnumerable where K : IEquatable<K>
    {
        private K[] m_Keys = null;
        private V[] m_Values = null;
        private int m_NextFreeIndex = 0;

        public Pool(int count)
        {
            this.m_Keys = new K[count];
            this.m_Values = new V[count];
        }

        private void UpdateNextFreeIndex()
        {
            for (int i = this.m_NextFreeIndex + 1; i < this.m_Values.Length; i++)
                if (this.m_Values[i] == null)
                {
                    this.m_NextFreeIndex = i;
                    return;
                }
            for (int i = 0; i < this.m_NextFreeIndex - 1; i++)
                if (this.m_Values[i] == null)
                {
                    this.m_NextFreeIndex = i;
                    return;
                }
            Console.WriteLine("No more space left in pool.");
            this.m_NextFreeIndex = -1;
        }

        public int Store(K key, V value)
        {
            if (this.m_NextFreeIndex == -1)
                throw new InvalidOperationException("Pool is out of memory.");
            if (value == null)
                throw new ArgumentNullException("value");
            if (key == null)
                throw new ArgumentNullException("key");
            this.m_Keys[this.m_NextFreeIndex] = key;
            this.m_Values[this.m_NextFreeIndex] = value;
            int result = this.m_NextFreeIndex;
            Console.WriteLine("Pool allocating object at index " + result + ".");
            this.UpdateNextFreeIndex();
            return result;
        }

        public V Free(int idx)
        {
            V obj = this.m_Values[idx];
            if (obj == null)
                throw new InvalidOperationException("No such object to free.");
            this.m_Values[idx] = default(V);
            Console.WriteLine("Pool freeing object at index " + idx + ".");
            return obj;
        }

        public bool Has(K key)
        {
            for (int i = 0; i < this.m_Keys.Length; i++)
                if (this.m_Keys[i] != null && this.m_Keys[i].Equals(key))
                    return true;
            return false;
        }

        public V this[int i]
        {
            get
            {
                if (this.m_Values[i] == null)
                    throw new InvalidOperationException("No such object at specified index.");
                return this.m_Values[i];
            }
        }

        public int this[K key]
        {
            get
            {
                for (int i = 0; i < this.m_Keys.Length; i++)
                    if (this.m_Keys[i] != null && this.m_Keys[i].Equals(key))
                        return i;
                throw new InvalidOperationException("Object with associated key not found in pool.");
            }
        }

        #region IEnumerable Members

        public virtual IEnumerator GetEnumerator()
        {
            for (int i = 0; i < this.m_Values.Length; i++)
                if (this.m_Values[i] != null)
                    yield return this.m_Values[i];
        }

        #endregion
    }
}
