using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XboxFPSGame.Static;
using XboxFPSGame.Dynamic;
using XboxFPSGame.Debug;
using BulletXNA.LinearMath;
using BulletXNA.BulletCollision;
using BulletXNA.BulletDynamics;
using BulletXNA;

namespace XboxFPSGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
#if SHOW_ZERO_POINT
        RenderTarget2D m_TextTarget = null;
#endif

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
            Content.RootDirectory = "Content";
            Components.Add(new FPSCounterGameComponent(this, Window));
        }

        public GameState State
        {
            get;
            private set;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            this.State = new GameState(this, this.GraphicsDevice, new SpriteBatch(GraphicsDevice));
            this.State.Textures.Add("CubetacularFrontTexture", Content.Load<Texture2D>("CubetacularFrontTexture"));
            this.State.Textures.Add("CubetacularBackTexture", Content.Load<Texture2D>("CubetacularBackTexture"));
            this.State.Textures.Add("CubetacularTopTexture", Content.Load<Texture2D>("CubetacularTopTexture"));
            this.State.Textures.Add("CubetacularBottomTexture", Content.Load<Texture2D>("CubetacularBottomTexture"));
            this.State.Textures.Add("CubetacularLeftTexture", Content.Load<Texture2D>("CubetacularLeftTexture"));
            this.State.Textures.Add("CubetacularRightTexture", Content.Load<Texture2D>("CubetacularRightTexture"));
            this.State.Fonts.Add("Arial", Content.Load<SpriteFont>("Arial"));
            this.State.Level = new Level(this.State);

            // TODO: use this.Content to load your game content here
#if SHOW_ZERO_POINT
            // Create text that appears in center of cube for debugging.
            this.m_TextTarget = new RenderTarget2D(
                this.State.GraphicsDevice,
                (int)this.State.Fonts["Arial"].MeasureString("0").X,
                (int)this.State.Fonts["Arial"].MeasureString("0").Y,
                true,
                this.State.GraphicsDevice.DisplayMode.Format,
                DepthFormat.Depth24, /* todo make this match the graphics device */
                this.State.GraphicsDevice.PresentationParameters.MultiSampleCount,
                RenderTargetUsage.DiscardContents
                );

            this.State.GraphicsDevice.SetRenderTarget(this.m_TextTarget);
            this.State.SpriteBatch.Begin(/*SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None*/);
            this.State.SpriteBatch.DrawString(this.State.Fonts["Arial"], "0", new Vector2(0, 0), Color.Red);
            this.State.SpriteBatch.End();
            this.State.GraphicsDevice.SetRenderTarget(null);
#endif
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            this.State.DynamicsWorld.StepSimulation((float)gameTime.ElapsedGameTime.TotalSeconds, 1);
            this.State.Player.Update(gameTime);
            base.Update(gameTime);
        }

        private XNA_ShapeDrawer m_shapeDrawer;
        private IDebugDraw m_debugDraw;
        private IndexedMatrix m_lookAt;
        private IndexedMatrix m_perspective;

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // Render the player and level
            this.State.Player.Draw(gameTime);
            this.State.Level.Draw(gameTime);

            XNA_ShapeDrawer sd = (XNA_ShapeDrawer)this.State.DynamicsWorld.GetDebugDrawer();
            m_shapeDrawer = sd;
            m_debugDraw = sd;
            m_lookAt = this.State.Camera.LookAtMatrix;
            m_perspective = this.State.Camera.ProjectionMatrix;

            this.State.DynamicsWorld.DebugDrawWorld();
            RenderScenePass(0, gameTime);
            IndexedVector3 location = new IndexedVector3(10, 10, 0);
            IndexedVector3 colour = new IndexedVector3(1, 1, 1);
            m_shapeDrawer.DrawText(String.Format("Memory [{0}]", System.GC.GetTotalMemory(false)), location, colour);
            m_shapeDrawer.RenderOthers(gameTime, m_lookAt, m_perspective);

            //this.GameState.CollisionWorld.DebugDrawWorld();

#if SHOW_ZERO_POINT
            Vector3 ScreenCoordinates = this.State.GraphicsDevice.Viewport.Project(Vector3.Zero, this.State.Camera.ProjectionMatrix, this.State.Camera.ViewMatrix, this.State.Camera.WorldMatrix);
            this.State.SpriteBatch.Begin(/*SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState*/);
            this.State.SpriteBatch.Draw(
                this.m_TextTarget,
                new Rectangle(
                    (int)ScreenCoordinates.X - this.m_TextTarget.Width / 2,
                    (int)ScreenCoordinates.Y - this.m_TextTarget.Height / 2,
                    this.m_TextTarget.Width,
                    this.m_TextTarget.Height
                ),
                Color.White
            );
            this.State.SpriteBatch.End();
            this.State.GraphicsDevice.BlendState = BlendState.Opaque;
            this.State.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
#endif

            base.Draw(gameTime);
        }


        protected virtual void RenderScenePass(int pass, GameTime gameTime)
        {
            IndexedMatrix m = IndexedMatrix.Identity;
            IndexedBasisMatrix rot = IndexedBasisMatrix.Identity;
            int numObjects = this.State.DynamicsWorld.GetNumCollisionObjects();
            IndexedVector3 wireColor = new IndexedVector3(1, 0, 0);

            for (int i = 0; i < numObjects; i++)
            {
                CollisionObject colObj = this.State.DynamicsWorld.GetCollisionObjectArray()[i];
                RigidBody body = RigidBody.Upcast(colObj);
                if (body != null && body.GetMotionState() != null)
                {
                    DefaultMotionState myMotionState = (DefaultMotionState)body.GetMotionState();
                    //myMotionState.m_graphicsWorldTrans.getOpenGLMatrix(m);
                    m = myMotionState.m_graphicsWorldTrans;
                    rot = myMotionState.m_graphicsWorldTrans._basis;
                }
                else
                {
                    //colObj.getWorldTransform().getOpenGLMatrix(m);
                    m = colObj.GetWorldTransform();
                    rot = colObj.GetWorldTransform()._basis;
                }
                wireColor = new IndexedVector3(1.0f, 1.0f, 0.5f); //wants deactivation
                if ((i & 1) != 0) wireColor = new IndexedVector3(0f, 0f, 1f);
                ///color differently for active, sleeping, wantsdeactivation states
                if (colObj.GetActivationState() == ActivationState.ACTIVE_TAG) //active
                {
                    if ((i & 1) != 0)
                    {
                        wireColor += new IndexedVector3(1f, 0f, 0f);
                    }
                    else
                    {
                        wireColor += new IndexedVector3(.5f, 0f, 0f);
                    }
                }
                if (colObj.GetActivationState() == ActivationState.ISLAND_SLEEPING) //ISLAND_SLEEPING
                {
                    if ((i & 1) != 0)
                    {
                        wireColor += new IndexedVector3(0f, 1f, 0f);
                    }
                    else
                    {
                        wireColor += new IndexedVector3(0f, 05f, 0f);
                    }
                }

                IndexedVector3 min, max;
                this.State.DynamicsWorld.GetBroadphase().GetBroadphaseAabb(out min, out max);

                min -= MathUtil.MAX_VECTOR;
                max += MathUtil.MAX_VECTOR;
                //		printf("aabbMin=(%f,%f,%f)\n",aabbMin.getX(),aabbMin.getY(),aabbMin.getZ());
                //		printf("aabbMax=(%f,%f,%f)\n",aabbMax.getX(),aabbMax.getY(),aabbMax.getZ());
                //		m_dynamicsWorld.getDebugDrawer().drawAabb(aabbMin,aabbMax,btVector3(1,1,1));

                switch (pass)
                {
                    case 0:
                        {
                            m_shapeDrawer.DrawXNA(ref m, colObj.GetCollisionShape(), ref wireColor, m_debugDraw.GetDebugMode(), ref min, ref max, ref m_lookAt, ref m_perspective);
                            break;
                        }
                    case 1:
                        {
                            IndexedVector3 shadow = rot * new IndexedVector3(1, 1, 1);
                            m_shapeDrawer.DrawShadow(ref m, ref shadow, colObj.GetCollisionShape(), ref min, ref max);
                            break;
                        }
                    case 2:
                        {
                            IndexedVector3 adjustedWireColor = wireColor * 0.3f;
                            m_shapeDrawer.DrawXNA(ref m, colObj.GetCollisionShape(), ref adjustedWireColor, 0, ref min, ref max, ref m_lookAt, ref m_perspective);
                            break;
                        }
                }
            }

            switch (pass)
            {
                case 0:
                    {
                        //m_shapeDrawer.RenderShadow(gameTime, ref m_lookAt, ref m_perspective);
                        m_shapeDrawer.RenderStandard(gameTime, ref m_lookAt, ref m_perspective);
                        m_shapeDrawer.RenderDebugLines(gameTime, ref m_lookAt, ref m_perspective);
                        break;
                    }
                case 1:
                    {
                        //m_shapeDrawer.RenderShadow(gameTime, ref m_lookAt, ref m_perspective);
                        break;
                    }
                case 2:
                    {
                        m_shapeDrawer.RenderStandard(gameTime, ref m_lookAt, ref m_perspective);
                        break;
                    }
            }
        }
    }
}
