﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace XboxFPSGame
{
    /// 
    /// This is a game component that implements IUpdateable.
    /// 
    /// <remarks>
    /// From http://www.gamedev.net/topic/593231-fps-counter-in-a-gamecomponent/.
    /// </remarks>
    public class FPSCounterGameComponent : DrawableGameComponent
    {
        private float fps;
        private float updateInterval = 1.0f;
        private float timeSinceLastUpdate = 0.0f;
        private float framecount = 0;
        private GameWindow Window;

        public FPSCounterGameComponent(Game game, GameWindow Window)
            : base(game)
        {
            // TODO: Construct any child components here
            this.Window = Window;
        }

        /// 
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// 
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// 
        /// Allows the game component to update itself.
        /// 
        /// Provides a snapshot of timing values.
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            framecount++;
            timeSinceLastUpdate += elapsed;
            if (timeSinceLastUpdate > updateInterval)
            {
                fps = framecount / timeSinceLastUpdate; //mean fps over updateIntrval
#if XBOX360
                System.Diagnostics.Debug.WriteLine("FPS: " + fps.ToString() + " - RT: " + gameTime.ElapsedGameTime.TotalSeconds.ToString() + " - GT: " + gameTime.ElapsedGameTime.TotalSeconds.ToString());
#else
                Window.Title = "FPS: " + fps.ToString();
#endif
                framecount = 0;
                timeSinceLastUpdate -= updateInterval;
            }

            base.Draw(gameTime);
        }
    }
}
