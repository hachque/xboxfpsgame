﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XboxFPSGame
{
    public class ManualRectangle
    {
        private const int NUM_VERTICES = 6;
        private const int NUM_TRIANGLES = 2;

        private class RectangleDimension : IEquatable<RectangleDimension>
        {
            private Vector3 m_TopLeft;
            private Vector3 m_BottomRight;

            public RectangleDimension(Vector3 topLeft, Vector3 bottomRight)
            {
                this.m_TopLeft = topLeft;
                this.m_BottomRight = bottomRight;
            }

            #region IEquatable<RectangleDimension> Members

            public bool Equals(RectangleDimension other)
            {
                return (this.m_TopLeft == other.m_TopLeft &&
                    this.m_BottomRight == other.m_BottomRight);
            }

            #endregion
        }

        private static Pool<RectangleDimension, VertexBuffer> m_VertexBuffers = new Pool<RectangleDimension, VertexBuffer>(10000);

        private Vector3 m_TopLeft;
        private Vector3 m_TopRight;
        private Vector3 m_BottomLeft;
        private Vector3 m_BottomRight;
        private Vector3 m_TextureSize;
        private Vector3 m_Normal;
        private int m_VertexBufferIndex;
        private VertexPositionNormalTexture[] m_Vertices;
        private GraphicsDevice m_Device;
        private Matrix m_Translation;
        private bool m_Flipped;

        public ManualRectangle(GraphicsDevice device, Vector3 topLeft, Vector3 topRight, Vector3 bottomLeft, Vector3 bottomRight, Vector3 textureSize, Vector3 normal, bool flipped)
        {
            this.m_TopLeft = topLeft;
            this.m_TopRight = topRight;
            this.m_BottomLeft = bottomLeft;
            this.m_BottomRight = bottomRight;
            this.m_TextureSize = textureSize;
            this.m_Normal = normal;
            this.m_Device = device;
            this.m_Flipped = flipped;
            this.m_Translation = Matrix.CreateTranslation(topLeft);

            // Get a vertex buffer from the pool if one already matches.
            RectangleDimension dim = new RectangleDimension(this.m_TopLeft, this.m_BottomRight);
            if (ManualRectangle.m_VertexBuffers.Has(dim))
                this.m_VertexBufferIndex = ManualRectangle.m_VertexBuffers[dim];
            else
                this.ConstructRectangle();
        }

        private void ConstructRectangle()
        {
            // Construct the 3D rectangle.
            this.m_Vertices = new VertexPositionNormalTexture[NUM_VERTICES];

            // UV texture coordinates
            Vector2 textureTopLeft = new Vector2(1.0f / this.m_TextureSize.X, 0.0f / this.m_TextureSize.Y);
            Vector2 textureTopRight = new Vector2(0.0f / this.m_TextureSize.X, 0.0f / this.m_TextureSize.Y);
            Vector2 textureBottomLeft = new Vector2(1.0f / this.m_TextureSize.X, 1.0f / this.m_TextureSize.Y);
            Vector2 textureBottomRight = new Vector2(0.0f / this.m_TextureSize.X, 1.0f / this.m_TextureSize.Y);

            // Add the vertices for the FRONT face.
            this.m_Vertices[0] = new VertexPositionNormalTexture(this.m_TopLeft, this.m_Normal, textureTopLeft);
            this.m_Vertices[1] = new VertexPositionNormalTexture(this.m_BottomLeft, this.m_Normal, textureBottomLeft);
            this.m_Vertices[2] = new VertexPositionNormalTexture(this.m_TopRight, this.m_Normal, textureTopRight);
            this.m_Vertices[3] = new VertexPositionNormalTexture(this.m_BottomLeft, this.m_Normal, textureBottomLeft);
            this.m_Vertices[4] = new VertexPositionNormalTexture(this.m_BottomRight, this.m_Normal, textureBottomRight);
            this.m_Vertices[5] = new VertexPositionNormalTexture(this.m_TopRight, this.m_Normal, textureTopRight);

            // Flip if needed.
            if (this.m_Flipped)
                for (int i = 0; i < NUM_VERTICES; i++)
                    this.m_Vertices[i].Normal *= -1;

            // Create the shape buffer and dispose of it to prevent out of memory
            this.m_VertexBufferIndex = ManualRectangle.m_VertexBuffers.Store(
                new RectangleDimension(this.m_TopLeft, this.m_BottomRight),
                new VertexBuffer(this.m_Device,
                    VertexPositionNormalTexture.VertexDeclaration,
                    NUM_VERTICES,
                    BufferUsage.WriteOnly)
                );
            ManualRectangle.m_VertexBuffers[this.m_VertexBufferIndex].SetData(this.m_Vertices);
        }

        /// <summary>
        /// Writes our list of vertices to the vertex buffer, 
        /// then draws triangles to the device
        /// </summary>
        /// <param name="device"></param>
        public void Render()
        {
            // Draw the primitives from the vertex buffer to the device as triangles
            this.m_Device.SetVertexBuffer(ManualRectangle.m_VertexBuffers[this.m_VertexBufferIndex]);
            this.m_Device.DrawPrimitives(PrimitiveType.TriangleList, 0, NUM_TRIANGLES);    
        }
    }
}
