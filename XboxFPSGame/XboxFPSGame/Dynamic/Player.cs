﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XboxFPSGame.Dynamic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using BulletXNA.LinearMath;
using BulletXNA.BulletCollision;
using BulletXNA.BulletDynamics;

namespace XboxFPSGame
{
    public class Player : Drawable
    {
        private const float MOVE_SPEED = 0.035f;
        private const float STRAFE_SPEED = 0.0125f;
        public const float HEAD_HEIGHT = 0.5f;
        private const float HEAD_BOB_SPEED = 0.25f;

        private double m_HeadBobRotation = 0.0f;
        private KinematicCharacterController m_CollisionController;
        private PairCachingGhostObject m_CollisionGhostObject;
        private ConvexShape m_CollisionShape;
        
        public Camera Camera
        {
            get;
            private set;
        }

        public Player(GameState state)
            : base(state)
        {
            this.Camera = this.GameState.Camera;

            
            /*
	m_ghostObject = new btPairCachingGhostObject();
	m_ghostObject->setWorldTransform(startTransform);
	sweepBP->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
	btScalar characterHeight=1.75;
	btScalar characterWidth =1.75;
	btConvexShape* capsule = new btCapsuleShape(characterWidth,characterHeight);
	m_ghostObject->setCollisionShape (capsule);
	m_ghostObject->setCollisionFlags (btCollisionObject::CF_CHARACTER_OBJECT);

	btScalar stepHeight = btScalar(0.35);
	m_character = new btKinematicCharacterController (m_ghostObject,capsule,stepHeight);
            */

            // Configure sweep.
            //IndexedVector3 min = new IndexedVector3(-100, -100, -100);
            //IndexedVector3 max = new IndexedVector3(100, 100, 100);
            //AxisSweep3Internal sweep = new AxisSweep3Internal();

            // Set up character collision.
            this.m_CollisionShape = new CapsuleShape(0.1f, 0.25f);
            this.m_CollisionGhostObject = new PairCachingGhostObject();
            this.m_CollisionGhostObject.SetWorldTransform(Matrix.CreateTranslation(new Vector3(0, 0, 0)));
            //sweep.SetOverlappingPairUserCallback(new GhostPairCallback());
            //sweepBP->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
            this.m_CollisionGhostObject.SetCollisionShape(this.m_CollisionShape);
            this.m_CollisionGhostObject.SetCollisionFlags(CollisionFlags.CF_CHARACTER_OBJECT);
            this.m_CollisionController = new KinematicCharacterController(this.m_CollisionGhostObject, this.m_CollisionShape, 0.05f, 1);
            this.m_CollisionController.SetGravity(0f);
            //this.m_CollisionController.SetJumpSpeed(0.2f);
            //this.m_CollisionController.SetFallSpeed(0.02f);

            // Warp player.
            IndexedVector3 target = this.Camera.Position;
            this.m_CollisionController.Reset();
            this.m_CollisionController.Warp(ref target);

            // Add to dynamics.
            this.GameState.DynamicsWorld.AddCollisionObject(this.m_CollisionGhostObject, CollisionFilterGroups.CharacterFilter, CollisionFilterGroups.StaticFilter | CollisionFilterGroups.DefaultFilter);
            this.GameState.DynamicsWorld.AddAction(this.m_CollisionController);
        }

        public override void Update(GameTime time)
        {
            this.Camera.Update(time);

            // Update camera position to character position.
            //IndexedVector3 zero = IndexedVector3.Zero;
            //this.m_CollisionController.SetWalkDirection(ref zero);
            this.Camera.SyncPosition(this.m_CollisionGhostObject.GetWorldTransform());

            KeyboardState keystate = Keyboard.GetState();

            // Sane movement.
            Vector3 sane = Vector3.Zero;
            if (keystate.IsKeyDown(Keys.W))
                sane += new Vector3(0, 0, -MOVE_SPEED);
            if (keystate.IsKeyDown(Keys.S))
                sane += new Vector3(0, 0, MOVE_SPEED);
            if (keystate.IsKeyDown(Keys.A))
                sane += new Vector3(-STRAFE_SPEED, 0, 0);
            if (keystate.IsKeyDown(Keys.D))
                sane += new Vector3(STRAFE_SPEED, 0, 0);
            this.AttemptMove(sane);

            // Direct movement.
            if (keystate.IsKeyDown(Keys.Up))
                this.Camera.AddToCameraPosition(new Vector3(0, 0, -MOVE_SPEED));
            if (keystate.IsKeyDown(Keys.Down))
                this.Camera.AddToCameraPosition(new Vector3(0, 0, MOVE_SPEED));
            if (keystate.IsKeyDown(Keys.Left))
                this.Camera.AddToCameraPosition(new Vector3(-MOVE_SPEED, 0, 0));
            if (keystate.IsKeyDown(Keys.Right))
                this.Camera.AddToCameraPosition(new Vector3(MOVE_SPEED, 0, 0));

            // Debug block.
            /*
            if (keystate.IsKeyDown(Keys.D1))
                this.GameState.Level.m_Blocks[0].DisableSide(AutomaticCube.SIDE_FRONT);
            else
                this.GameState.Level.m_Blocks[0].EnableSide(AutomaticCube.SIDE_FRONT);
            if (keystate.IsKeyDown(Keys.D2))
                this.GameState.Level.m_Blocks[0].DisableSide(AutomaticCube.SIDE_BACK);
            else
                this.GameState.Level.m_Blocks[0].EnableSide(AutomaticCube.SIDE_BACK);
            if (keystate.IsKeyDown(Keys.D3))
                this.GameState.Level.m_Blocks[0].DisableSide(AutomaticCube.SIDE_TOP);
            else
                this.GameState.Level.m_Blocks[0].EnableSide(AutomaticCube.SIDE_TOP);
            if (keystate.IsKeyDown(Keys.D4))
                this.GameState.Level.m_Blocks[0].DisableSide(AutomaticCube.SIDE_BOTTOM);
            else
                this.GameState.Level.m_Blocks[0].EnableSide(AutomaticCube.SIDE_BOTTOM);
            if (keystate.IsKeyDown(Keys.D5))
                this.GameState.Level.m_Blocks[0].DisableSide(AutomaticCube.SIDE_LEFT);
            else
                this.GameState.Level.m_Blocks[0].EnableSide(AutomaticCube.SIDE_LEFT);
            if (keystate.IsKeyDown(Keys.D6))
                this.GameState.Level.m_Blocks[0].DisableSide(AutomaticCube.SIDE_RIGHT);
            else
                this.GameState.Level.m_Blocks[0].EnableSide(AutomaticCube.SIDE_RIGHT);
            */

            if (keystate.IsKeyDown(Keys.Up) || keystate.IsKeyDown(Keys.Down) ||
                keystate.IsKeyDown(Keys.Left) || keystate.IsKeyDown(Keys.Right))
                this.m_HeadBobRotation += HEAD_BOB_SPEED;
            this.Camera.HeadBob = 0f + Math.Sin(this.m_HeadBobRotation) / 100f;
        }

        private void AttemptMove(Vector3 desired)
        {
            IndexedVector3 vec = this.Camera.GetLateralLookAt(desired);
            this.m_CollisionController.SetWalkDirection(ref vec);
            //this.m_CollisionController.SetMaxSlope(MathHelper.PiOver4);
            //this.m_CollisionController.SetGravity(0);

            /*
            IndexedVector3 from = this.Camera.Position;
            IndexedVector3 to = this.Camera.GetNextCameraPosition(desired);

            ClosestRayResultCallback callback = new ClosestRayResultCallback(ref from, ref to);
            this.GameState.CollisionWorld.RayTest(ref from, ref to, callback);

            if (callback.HasHit())
            {
                //this.Camera.AddToCameraPositionLaterally(from - callback.m_hitPointWorld);
                //Console.WriteLine("Moved into collision wall from " + from + " to " + this.Camera.Position);
            }
            else
            {
                this.Camera.AddToCameraPosition(desired);
                //this.m_CollisionObject.SetWorldTransform(IndexedMatrix.CreateTranslation(this.Camera.Position));
                //if (this.Camera.Position != (Vector3)to)
                //    throw new Exception("Invalid!");
            }*/
        }

        public override void Draw(GameTime time)
        {
            this.Camera.Draw(time);
        }
    }
}
