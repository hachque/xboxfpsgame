﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XboxFPSGame.Dynamic
{
    public class Camera : Drawable
    {
        private const float ROTATION_SPEED = 0.004f;

        private float m_UpDownRotation;
        private float m_LeftRightRotation;
        private MouseState m_PreviousMouseState;

        public Camera(GameState state, Vector3 position) : base(state)
        {
            this.Position = position;
            this.WorldMatrix = Matrix.CreateTranslation(Vector3.Zero);
            this.ViewMatrix = new Matrix();
            this.ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.PiOver4,
                this.GameState.GraphicsDevice.Viewport.AspectRatio,
                0.02f,
                50
                );
            this.UpdateViewMatrix();
        }

        public override void Update(GameTime time)
        {
            MouseState current = Mouse.GetState();

            if (current != this.m_PreviousMouseState)
            {
                float xDifference = current.X - this.m_PreviousMouseState.X;
                float yDifference = current.Y - this.m_PreviousMouseState.Y;
                this.m_LeftRightRotation -= ROTATION_SPEED * xDifference;
                this.m_UpDownRotation -= ROTATION_SPEED * yDifference;
                this.m_UpDownRotation = Math.Max(Math.Min(MathHelper.Pi / 2, this.m_UpDownRotation), -MathHelper.Pi / 2);
                Mouse.SetPosition(this.GameState.GraphicsDevice.Viewport.Width / 2, this.GameState.GraphicsDevice.Viewport.Height / 2);
                this.m_PreviousMouseState = Mouse.GetState();
            }

            this.UpdateViewMatrix();
        }

        public override void Draw(GameTime time)
        {
        }

        public Vector3 Position
        {
            get;
            private set;
        }

        public double HeadBob
        {
            get;
            set;
        }

        public Matrix ViewMatrix
        {
            get;
            private set;
        }

        public Matrix LookAtMatrix
        {
            get
            {
                Matrix applied = Matrix.CreateRotationX(this.m_UpDownRotation) * Matrix.CreateRotationY(this.m_LeftRightRotation);
                Vector3 target = new Vector3(0, 0, -1);
                Vector3 upvec = new Vector3(0, 1, 0);
                Vector3 rotated = Vector3.Transform(target, applied);
                target = this.AdjustedPosition + rotated;
                upvec = Vector3.Transform(upvec, applied);
                return Matrix.CreateLookAt(this.AdjustedPosition, target, upvec);
            }
        }

        public Matrix ProjectionMatrix
        {
            get;
            private set;
        }

        public Matrix WorldMatrix
        {
            get;
            private set;
        }

        private Vector3 AdjustedPosition
        {
            get
            {
                return this.Position + new Vector3(0, (float)HeadBob, 0);
            }
        }

        private void UpdateViewMatrix()
        {
            Matrix applied = Matrix.CreateRotationX(this.m_UpDownRotation) * Matrix.CreateRotationY(this.m_LeftRightRotation);
            Vector3 target = new Vector3(0, 0, -1);
            Vector3 upvec = new Vector3(0, 1, 0);
            Vector3 rotated = Vector3.Transform(target, applied);
            target = this.AdjustedPosition + rotated;
            upvec = Vector3.Transform(upvec, applied);
            this.ViewMatrix = Matrix.CreateLookAt(this.AdjustedPosition, target, upvec);
        }

        public void AddToCameraPosition(Vector3 add)
        {
            Matrix cameraRotation = Matrix.CreateRotationX(this.m_UpDownRotation) * Matrix.CreateRotationY(this.m_LeftRightRotation);
            Vector3 rotatedVector = Vector3.Transform(add, cameraRotation);
            this.Position += rotatedVector;
            UpdateViewMatrix();
        }

        public void AddToCameraPositionLaterally(Vector3 add)
        {
            Matrix cameraRotation = Matrix.CreateRotationY(this.m_LeftRightRotation);
            Vector3 rotatedVector = Vector3.Transform(add, cameraRotation);
            this.Position += rotatedVector;
            UpdateViewMatrix();
        }

        public Vector3 GetNextCameraPosition(Vector3 add)
        {
            Matrix cameraRotation = Matrix.CreateRotationX(this.m_UpDownRotation) * Matrix.CreateRotationY(this.m_LeftRightRotation);
            Vector3 rotatedVector = Vector3.Transform(add, cameraRotation);
            return this.Position + rotatedVector;
        }

        public Vector3 GetNextCameraPositionLaterally(Vector3 add)
        {
            Matrix cameraRotation = Matrix.CreateRotationY(this.m_LeftRightRotation);
            Vector3 rotatedVector = Vector3.Transform(add, cameraRotation);
            return this.Position + rotatedVector;
        }

        public Vector3 GetLookAt(Vector3 add)
        {
            Matrix cameraRotation = Matrix.CreateRotationX(this.m_UpDownRotation) * Matrix.CreateRotationY(this.m_LeftRightRotation);
            Vector3 rotatedVector = Vector3.Transform(add, cameraRotation);
            return rotatedVector;
        }

        public Vector3 GetLateralLookAt(Vector3 add)
        {
            Matrix cameraRotation = Matrix.CreateRotationY(this.m_LeftRightRotation);
            Vector3 rotatedVector = Vector3.Transform(add, cameraRotation);
            rotatedVector.Y = 0;
            return rotatedVector;
        }

        public void SyncPosition(Matrix matrix)
        {
            this.Position = matrix.Translation;
        }
    }
}
