﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XboxFPSGame.Dynamic;
using BulletXNA.BulletCollision;
using BulletXNA.LinearMath;
using BulletXNA.BulletDynamics;
using XboxFPSGame.Static;

namespace XboxFPSGame
{
    public class GameState
    {
        public GameState(Game game, GraphicsDevice graphics, SpriteBatch spriteBatch)
        {
            this.GraphicsDevice = graphics;
            this.SpriteBatch = spriteBatch;
            this.Textures = new Dictionary<string, Texture2D>();
            this.Fonts = new Dictionary<string, SpriteFont>();
            this.Camera = new Camera(this, new Vector3(0.5f, 0 + Player.HEAD_HEIGHT, 0.5f));

            // Set up collision system.
            IBroadphaseInterface bi = new DbvtBroadphase();
            IOverlappingPairCallback gpc = new GhostPairCallback();
            bi.GetOverlappingPairCache().SetInternalGhostPairCallback(gpc);
            DefaultCollisionConfiguration dcc = new DefaultCollisionConfiguration();
            CollisionDispatcher cd = new CollisionDispatcher(dcc);
            SequentialImpulseConstraintSolver cs = new SequentialImpulseConstraintSolver();
            this.DynamicsWorld = new DiscreteDynamicsWorld(cd, bi, cs, dcc);
            IndexedVector3 grav = new IndexedVector3(0, -0.001f, 0);
            this.DynamicsWorld.SetGravity(ref grav);

            // Set up debug drawer.
            XboxFPSGame.Debug.XNA_ShapeDrawer xna = new XboxFPSGame.Debug.XNA_ShapeDrawer(game, this.Camera);
            xna.LoadContent();
            xna.SetDebugMode(DebugDrawModes.ALL);
            this.DynamicsWorld.SetDebugDrawer(xna);

            // Set up player.
            this.Player = new Player(this);
        }

        ~GameState()
        {
            this.DynamicsWorld.Cleanup();
        }

        public Camera Camera
        {
            get;
            private set;
        }

        public Player Player
        {
            get;
            private set;
        }

        public Level Level
        {
            get;
            set;
        }

        public GraphicsDevice GraphicsDevice
        {
            get;
            private set;
        }

        public SpriteBatch SpriteBatch
        {
            get;
            private set;
        }

        public DiscreteDynamicsWorld DynamicsWorld
        {
            get;
            private set;
        }

        public Dictionary<string, Texture2D> Textures
        {
            get;
            private set;
        }

        public Dictionary<string, SpriteFont> Fonts
        {
            get;
            private set;
        }
    }
}
