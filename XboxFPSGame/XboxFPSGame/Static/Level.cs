﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XboxFPSGame.Debug;
using BulletXNA.LinearMath;
using BulletXNA.BulletCollision;
using BulletXNA;
using BulletXNA.BulletDynamics;

namespace XboxFPSGame.Static
{
    public class Level : Drawable
    {
        public List<Block> m_Blocks = new List<Block>();
        private Random m_Random = new Random();
        private BoundingFrustum m_Frustum = null;

        public Level(GameState state)
            : base(state)
        {
            for (int a = 0; a < 1; a++)
            {
                // Generate the first corridor.
                for (int i = 0; i < 10; i++)
                {
                    Block b = new Block(this.GameState, 0, BlockTypes.Default.Bind(this.GameState), 0, a, i);
                    if (i != 0)
                        b.DisableSide(AutomaticCube.SIDE_FRONT);
                    else
                        b.DisableSide(AutomaticCube.SIDE_RIGHT);
                    if (i != 9)
                        b.DisableSide(AutomaticCube.SIDE_BACK);
                    else
                        b.DisableSide(AutomaticCube.SIDE_RIGHT);
                    this.m_Blocks.Add(b);
                }

                // Generate the second corridor.
                for (int i = 0; i < 10; i++)
                {
                    Block b = new Block(this.GameState, 0, BlockTypes.Default.Bind(this.GameState), 9, a, i);
                    if (i != 0)
                        b.DisableSide(AutomaticCube.SIDE_FRONT);
                    else
                        b.DisableSide(AutomaticCube.SIDE_LEFT);
                    if (i != 9)
                        b.DisableSide(AutomaticCube.SIDE_BACK);
                    else
                        b.DisableSide(AutomaticCube.SIDE_LEFT);
                    this.m_Blocks.Add(b);
                }

                // Generate the third corridor.
                for (int i = 1; i < 9; i++)
                {
                    Block b = new Block(this.GameState, 0, BlockTypes.Default.Bind(this.GameState), i, a, 9);
                    b.DisableSide(AutomaticCube.SIDE_LEFT);
                    //if (i != 8)
                    b.DisableSide(AutomaticCube.SIDE_RIGHT);
                    this.m_Blocks.Add(b);
                }

                // Generate the fourth corridor.
                for (int i = 1; i < 9; i++)
                {
                    Block b = new Block(this.GameState, 0, BlockTypes.Default.Bind(this.GameState), i, a, 0);
                    b.DisableSide(AutomaticCube.SIDE_RIGHT);
                    //if (i != 8)
                    b.DisableSide(AutomaticCube.SIDE_LEFT);
                    this.m_Blocks.Add(b);
                }
            }
        }

        public override void Update(GameTime time)
        {
            //this.m_Frustum = new BoundingFrustum(this.GameState.Camera.ViewMatrix * this.GameState.Camera.ProjectionMatrix);

            //foreach (Block b in this.m_Blocks)
            //{
                //if (this.m_Frustum.Intersects(b.BoundingBox))
             //       b.Update(time);
            //}
        }

        public override void Draw(GameTime time)
        {
            foreach (Block b in this.m_Blocks)
            {
                if (this.m_Frustum == null || this.m_Frustum.Intersects(b.BoundingBox))
                    b.Draw(time);
            }
        }
    }
}
