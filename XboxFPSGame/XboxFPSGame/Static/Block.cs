﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BulletXNA.BulletCollision;
using BulletXNA.LinearMath;
using BulletXNA.BulletDynamics;
using BulletXNA;
using Microsoft.Xna.Framework.Input;

namespace XboxFPSGame.Static
{
    public class Block : Drawable
    {
        AutomaticCube m_Cube = null;
        BoundingBox m_BoundingBox;
        RigidBody[] m_CollisionBody = new RigidBody[6];
        CollisionShape[] m_CollisionShape = new CollisionShape[6];
        bool[] m_CollisionShapeAdded = new bool[6];

        public Block(GameState state, int id, BlockType type, int x, int y, int z)
            : base(state)
        {
            this.m_Cube = new AutomaticCube(state, id, new Vector3(0.5f, 0.5f, 0.5f), new Vector3(x, y, z), true, new Texture2D[]
                {
                    type.FrontTexture,
                    type.BackTexture,
                    type.TopTexture,
                    type.BottomTexture,
                    type.LeftTexture,
                    type.RightTexture,
                });
            this.m_BoundingBox = new BoundingBox(new Vector3(x, y, z), new Vector3(x + 1f, y + 1f, z + 1f));

            // Add to collision world.
            this.m_CollisionShape[AutomaticCube.SIDE_BACK] = new BoxShape(new IndexedVector3(0.5f, 0.5f, 0.01f));
            this.m_CollisionShape[AutomaticCube.SIDE_FRONT] = new BoxShape(new IndexedVector3(0.5f, 0.5f, 0.01f));
            this.m_CollisionShape[AutomaticCube.SIDE_LEFT] = new BoxShape(new IndexedVector3(0.01f, 0.5f, 0.5f));
            this.m_CollisionShape[AutomaticCube.SIDE_RIGHT] = new BoxShape(new IndexedVector3(0.01f, 0.5f, 0.5f));
            this.m_CollisionShape[AutomaticCube.SIDE_TOP] = new BoxShape(new IndexedVector3(0.5f, 0.01f, 0.5f));
            this.m_CollisionShape[AutomaticCube.SIDE_BOTTOM] = new BoxShape(new IndexedVector3(0.5f, 0.01f, 0.5f));
            for (int i = 0; i < 6; i++)
                this.m_CollisionBody[i] = new RigidBody(new RigidBodyConstructionInfo(0, new DefaultMotionState(), this.m_CollisionShape[i]));
            this.m_CollisionBody[AutomaticCube.SIDE_BACK].SetWorldTransform(IndexedMatrix.CreateTranslation(new IndexedVector3(x + 0.5f, y + 0.5f, z + 1)));
            this.m_CollisionBody[AutomaticCube.SIDE_FRONT].SetWorldTransform(IndexedMatrix.CreateTranslation(new IndexedVector3(x + 0.5f, y + 0.5f, z)));
            this.m_CollisionBody[AutomaticCube.SIDE_LEFT].SetWorldTransform(IndexedMatrix.CreateTranslation(new IndexedVector3(x, y + 0.5f, z + 0.5f)));
            this.m_CollisionBody[AutomaticCube.SIDE_RIGHT].SetWorldTransform(IndexedMatrix.CreateTranslation(new IndexedVector3(x + 1, y + 0.5f, z + 0.5f)));
            this.m_CollisionBody[AutomaticCube.SIDE_TOP].SetWorldTransform(IndexedMatrix.CreateTranslation(new IndexedVector3(x + 0.5f, y + 1, z + 0.5f)));
            this.m_CollisionBody[AutomaticCube.SIDE_BOTTOM].SetWorldTransform(IndexedMatrix.CreateTranslation(new IndexedVector3(x + 0.5f, y, z + 0.5f)));
            for (int i = 0; i < 6; i++)
                this.EnableSide(i);
            /*    this.GameState.DynamicsWorld.AddRigidBody(this.m_CollisionBody[i]);
            for (int i = 0; i < 6; i++)
                this.m_CollisionShapeAdded[i] = true;*/
        }

        ~Block()
        {
        }

        public override void Update(GameTime time)
        {
        }

        public override void Draw(GameTime time)
        {
            this.GameState.GraphicsDevice.RasterizerState = RasterizerState.CullClockwise;
            this.m_Cube.Draw(time);
            this.GameState.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;/*
            for (int i = 0; i < 6; i++)
            {
                if (!this.m_CollisionShapeAdded[i])
                    continue;
                IndexedVector3 col = new IndexedVector3(1, 1, 0);
                IndexedMatrix mat = this.m_CollisionBody[i].GetWorldTransform();
                this.GameState.DynamicsWorld.DebugDrawObject(ref mat, this.m_CollisionShape[i], ref col);
            }*/
        }

        public void EnableSide(int side)
        {
            this.m_Cube.EnableSide(side);
            if (!this.m_CollisionShapeAdded[side])
            {
                this.GameState.DynamicsWorld.AddRigidBody(this.m_CollisionBody[side]);
                this.m_CollisionShapeAdded[side] = true;
            }
        }

        public void DisableSide(int side)
        {
            this.m_Cube.DisableSide(side);
            if (this.m_CollisionShapeAdded[side])
            {
                this.GameState.DynamicsWorld.RemoveRigidBody(this.m_CollisionBody[side]);
                this.m_CollisionShapeAdded[side] = false;
            }
        }

        public BoundingBox BoundingBox
        {
            get { return this.m_BoundingBox; }
        }
    }
}
