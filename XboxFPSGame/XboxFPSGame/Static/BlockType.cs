﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace XboxFPSGame.Static
{
    public class BlockType
    {
        private GameState m_GameState = null;
        private string m_FrontTextureName = null;
        private string m_BackTextureName = null;
        private string m_TopTextureName = null;
        private string m_BottomTextureName = null;
        private string m_LeftTextureName = null;
        private string m_RightTextureName = null;
        private Texture2D m_FrontTexture = null;
        private Texture2D m_BackTexture = null;
        private Texture2D m_TopTexture = null;
        private Texture2D m_BottomTexture = null;
        private Texture2D m_LeftTexture = null;
        private Texture2D m_RightTexture = null;

        public BlockType(
            string frontTexture,
            string backTexture,
            string topTexture, 
            string bottomTexture, 
            string leftTexture, 
            string rightTexture)
        {
            if (BlockTypes.Locked)
                throw new InvalidOperationException("New block types can't be defined at runtime.");

            this.m_FrontTextureName = frontTexture;
            this.m_BackTextureName = backTexture;
            this.m_TopTextureName = topTexture;
            this.m_BottomTextureName = bottomTexture;
            this.m_LeftTextureName = leftTexture;
            this.m_RightTextureName = rightTexture;
        }

        private BlockType(
            GameState state,
            string frontTexture,
            string backTexture,
            string topTexture, 
            string bottomTexture, 
            string leftTexture, 
            string rightTexture)
        {
            if (!BlockTypes.Locked)
                throw new InvalidOperationException("Binding block types must only be done at runtime.");

            this.m_GameState = state;
            this.m_FrontTextureName = frontTexture;
            this.m_BackTextureName = backTexture;
            this.m_TopTextureName = topTexture;
            this.m_BottomTextureName = bottomTexture;
            this.m_LeftTextureName = leftTexture;
            this.m_RightTextureName = rightTexture;
            this.m_FrontTexture = state.Textures[frontTexture];
            this.m_BackTexture = state.Textures[backTexture];
            this.m_TopTexture = state.Textures[topTexture];
            this.m_BottomTexture = state.Textures[bottomTexture];
            this.m_LeftTexture = state.Textures[leftTexture];
            this.m_RightTexture = state.Textures[rightTexture];
        }

        public BlockType Bind(GameState state)
        {
            return new BlockType(
                state,
                this.m_FrontTextureName,
                this.m_BackTextureName,
                this.m_TopTextureName,
                this.m_BottomTextureName,
                this.m_LeftTextureName,
                this.m_RightTextureName
                );
        }

        public Texture2D FrontTexture
        {
            get
            {
                if (this.m_GameState == null)
                    throw new InvalidOperationException("Block types must be bound before they can be used.");

                return this.m_FrontTexture;
            }
        }

        public Texture2D BackTexture
        {
            get
            {
                if (this.m_GameState == null)
                    throw new InvalidOperationException("Block types must be bound before they can be used.");

                return this.m_BackTexture;
            }
        }

        public Texture2D TopTexture
        {
            get
            {
                if (this.m_GameState == null)
                    throw new InvalidOperationException("Block types must be bound before they can be used.");

                return this.m_TopTexture;
            }
        }

        public Texture2D BottomTexture
        {
            get
            {
                if (this.m_GameState == null)
                    throw new InvalidOperationException("Block types must be bound before they can be used.");

                return this.m_BottomTexture;
            }
        }

        public Texture2D LeftTexture
        {
            get
            {
                if (this.m_GameState == null)
                    throw new InvalidOperationException("Block types must be bound before they can be used.");

                return this.m_LeftTexture;
            }
        }

        public Texture2D RightTexture
        {
            get
            {
                if (this.m_GameState == null)
                    throw new InvalidOperationException("Block types must be bound before they can be used.");

                return this.m_RightTexture;
            }
        }
    }
}
