﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XboxFPSGame.Static
{
    public static class BlockTypes
    {
        public static BlockType Default = null;
        private static bool m_Locked = false;

        static BlockTypes()
        {
            BlockTypes.Default = new BlockType(
                    "CubetacularFrontTexture",
                    "CubetacularBackTexture",
                    "CubetacularTopTexture",
                    "CubetacularBottomTexture",
                    "CubetacularLeftTexture",
                    "CubetacularRightTexture"
                );
            BlockTypes.Locked = true;
        }

        public static bool Locked
        {
            get
            {
                return BlockTypes.m_Locked;
            }
            private set
            {
                BlockTypes.m_Locked = value;
            }
        }
    }
}
