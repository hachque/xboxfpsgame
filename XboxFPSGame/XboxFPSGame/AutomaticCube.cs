﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XboxFPSGame
{
    class AutomaticCube : Drawable
    {
        public const int NUM_SIDES = 6;
        public const int SIDE_FRONT = 0;
        public const int SIDE_BACK = 1;
        public const int SIDE_TOP = 2;
        public const int SIDE_BOTTOM = 3;
        public const int SIDE_LEFT = 4;
        public const int SIDE_RIGHT = 5;

        Texture2D[] m_Textures;
        ManualRectangle[] m_Sides = new ManualRectangle[NUM_SIDES] { null, null, null, null, null, null };
        bool[] m_Enabled = new bool[NUM_SIDES] { false, false, false, false, false, false };
        BasicEffect m_Effect = null;
#if RENDER_BLOCK_ID
        RenderTarget2D m_TextTarget = null;
#endif

        public Vector3 Size { get; set; }
        public Vector3 Position { get; set; }

        public AutomaticCube(GameState state, int id, Vector3 size, Vector3 position, bool flippedNormals, Texture2D[] textures)
            : base(state)
        {
            // Set properties.
            this.Size = size;
            this.Position = position;
            this.m_Textures = textures;
            this.m_Effect = new BasicEffect(this.GameState.GraphicsDevice);

#if RENDER_BLOCK_ID
            // Create text that appears in center of cube for debugging.
            this.m_TextTarget = new RenderTarget2D(
                state.GraphicsDevice,
                (int)state.Fonts["Arial"].MeasureString(id.ToString()).X,
                (int)state.Fonts["Arial"].MeasureString(id.ToString()).Y,
                true,
                state.GraphicsDevice.DisplayMode.Format,
                DepthFormat.Depth24, /* todo make this match the graphics device */
                state.GraphicsDevice.PresentationParameters.MultiSampleCount,
                RenderTargetUsage.DiscardContents
                );

            this.GameState.GraphicsDevice.SetRenderTarget(this.m_TextTarget);
            this.GameState.SpriteBatch.Begin(/*SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None*/);
            this.GameState.SpriteBatch.DrawString(state.Fonts["Arial"], id.ToString(), new Vector2(0, 0), Color.Red);
            this.GameState.SpriteBatch.End();
            this.GameState.GraphicsDevice.SetRenderTarget(null);
#endif

            // Calculate the position of the vertices on the top face.
            Vector3 topLeftFront = new Vector3(-1.0f, 1.0f, -1.0f) * Size + Size;
            Vector3 topLeftBack = new Vector3(-1.0f, 1.0f, 1.0f) * Size + Size;
            Vector3 topRightFront = new Vector3(1.0f, 1.0f, -1.0f) * Size + Size;
            Vector3 topRightBack = new Vector3(1.0f, 1.0f, 1.0f) * Size + Size;

            // Calculate the position of the vertices on the bottom face.
            Vector3 btmLeftFront = new Vector3(-1.0f, -1.0f, -1.0f) * Size + Size;
            Vector3 btmLeftBack = new Vector3(-1.0f, -1.0f, 1.0f) * Size + Size;
            Vector3 btmRightFront = new Vector3(1.0f, -1.0f, -1.0f) * Size + Size;
            Vector3 btmRightBack = new Vector3(1.0f, -1.0f, 1.0f) * Size + Size;

            // Normal vectors for each face (needed for lighting / display)
            Vector3 normalFront = new Vector3(0.0f, 0.0f, 1.0f) * Size;
            Vector3 normalBack = new Vector3(0.0f, 0.0f, -1.0f) * Size;
            Vector3 normalTop = new Vector3(0.0f, 1.0f, 0.0f) * Size;
            Vector3 normalBottom = new Vector3(0.0f, -1.0f, 0.0f) * Size;
            Vector3 normalLeft = new Vector3(-1.0f, 0.0f, 0.0f) * Size;
            Vector3 normalRight = new Vector3(1.0f, 0.0f, 0.0f) * Size;

            // Set faces.
           /*Console.WriteLine("Cube");
            Console.WriteLine("===============");
            Console.WriteLine("TLF: " + topLeftFront);
            Console.WriteLine("TLB: " + topLeftBack);
            Console.WriteLine("TRF: " + topRightFront);
            Console.WriteLine("TRB: " + topRightBack);
            Console.WriteLine("BLF: " + btmLeftFront);
            Console.WriteLine("BLB: " + btmLeftBack);
            Console.WriteLine("BRF: " + btmRightFront);
            Console.WriteLine("BRB: " + btmRightBack);
            Console.WriteLine("===============");*/
            this.m_Sides[SIDE_FRONT] = new ManualRectangle(state.GraphicsDevice, topLeftFront, topRightFront, btmLeftFront, btmRightFront, Vector3.One / 2, normalFront, flippedNormals);
            this.m_Sides[SIDE_BACK] = new ManualRectangle(state.GraphicsDevice, topRightBack, topLeftBack, btmRightBack, btmLeftBack, Vector3.One / 2, normalBack, flippedNormals);
            this.m_Sides[SIDE_LEFT] = new ManualRectangle(state.GraphicsDevice, topLeftBack, topLeftFront, btmLeftBack, btmLeftFront, Vector3.One / 2, normalLeft, flippedNormals);
            this.m_Sides[SIDE_RIGHT] = new ManualRectangle(state.GraphicsDevice, topRightFront, topRightBack, btmRightFront, btmRightBack, Vector3.One / 2, normalRight, flippedNormals);
            this.m_Sides[SIDE_TOP] = new ManualRectangle(state.GraphicsDevice, topLeftBack, topRightBack, topLeftFront, topRightFront, Vector3.One / 2, normalTop, flippedNormals);
            this.m_Sides[SIDE_BOTTOM] = new ManualRectangle(state.GraphicsDevice, btmRightBack, btmLeftBack, btmRightFront, btmLeftFront, Vector3.One / 2, normalBottom, flippedNormals);
        }

        public void EnableSide(int side)
        {
            if (side < 0 || side >= NUM_SIDES)
                throw new ArgumentOutOfRangeException("side");

            this.m_Enabled[side] = true;
        }

        public void DisableSide(int side)
        {
            if (side < 0 || side >= NUM_SIDES)
                throw new ArgumentOutOfRangeException("side");

            this.m_Enabled[side] = false;
        }

        public override void Update(GameTime time)
        {
        }

        public override void Draw(GameTime time)
        {
            Matrix world = this.GameState.Camera.WorldMatrix;
            world.Translation += this.Position;
            this.m_Effect.World = world;
            this.m_Effect.View = this.GameState.Camera.ViewMatrix;
            this.m_Effect.Projection = this.GameState.Camera.ProjectionMatrix;
            this.m_Effect.EnableDefaultLighting();
            this.m_Effect.TextureEnabled = true;

            for (int i = 0; i < NUM_SIDES; i++)
                if (this.m_Enabled[i])
                {
                    this.m_Effect.Texture = this.m_Textures[i];
                    foreach (EffectPass pass in this.m_Effect.CurrentTechnique.Passes)
                    {
                        pass.Apply();
                        this.m_Sides[i].Render();
                    }
                }

#if RENDER_BLOCK_ID
            Vector3 ScreenCoordinates = this.GameState.GraphicsDevice.Viewport.Project(this.Position, this.GameState.Camera.ProjectionMatrix, this.GameState.Camera.ViewMatrix, this.GameState.Camera.WorldMatrix);
            this.GameState.SpriteBatch.Begin(/*SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState*/);
            this.GameState.SpriteBatch.Draw(
                this.m_TextTarget,
                new Rectangle(
                    (int)ScreenCoordinates.X - this.m_TextTarget.Width / 2,
                    (int)ScreenCoordinates.Y - this.m_TextTarget.Height / 2,
                    this.m_TextTarget.Width,
                    this.m_TextTarget.Height
                ),
                Color.White
            );
            this.GameState.SpriteBatch.DrawString(this.GameState.Fonts["Arial"], "World: " + this.GameState.Camera.WorldMatrix.ToString(), new Vector2(8, 8), Color.White);
            this.GameState.SpriteBatch.DrawString(this.GameState.Fonts["Arial"], "View: " + this.GameState.Camera.ViewMatrix.ToString(), new Vector2(8, 32), Color.White);
            this.GameState.SpriteBatch.DrawString(this.GameState.Fonts["Arial"], "Projection: " + this.GameState.Camera.ProjectionMatrix.ToString(), new Vector2(8, 64), Color.White);
            this.GameState.SpriteBatch.End();
            this.GameState.GraphicsDevice.BlendState = BlendState.Opaque;
            this.GameState.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
#endif
        }
    }
}
